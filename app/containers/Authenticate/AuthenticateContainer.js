import React from 'react'
import PropTypes from 'prop-types'
import createReactClass from 'create-react-class'
import { Authenticate } from 'components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as userActionCreators from 'redux/modules/users'

const AuthenticateContainer = createReactClass({
  propTypes: {
    isFetching: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    fetchAndHandleAuthedUser: PropTypes.func.isRequired
  },
  // Access to react router
  contextTypes: {
    router: PropTypes.object.isRequired
  },
  handleAuth (e) {
    e.preventDefault()
    this.props.fetchAndHandleAuthedUser()
      .then(() => {
        this.context.router.history.replace('feed')
      })
  },
  render () {
    return (
      <Authenticate
        isFetching={this.props.isFetching}
        error={this.props.error}
        onAuth={this.handleAuth}/>
    )
  }
})

// Tell the component what state it should be worrying about
function mapStateToProps (state) {
  console.log('State', state)
  return {
    isFetching: state.isFetching,
    error: state.error
  }
}

// Bind action creators to dispatch so you don't have to call it all the time
function mapDispatchToProps (dispatch) {
  return bindActionCreators(userActionCreators, dispatch)
}

// Connect to redux
export default connect(mapStateToProps, mapDispatchToProps)(AuthenticateContainer)
