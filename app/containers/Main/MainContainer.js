import React from 'react'
import PropTypes from 'prop-types'
import createReactClass from 'create-react-class'
import { Navigation } from 'components'
import { connect } from 'react-redux'
import { container, innerContainer } from './styles.css'
import { withRouter } from 'react-router'

const MainContainer = createReactClass({
  propTypes: {
    children: PropTypes.any,
    isAuthed: PropTypes.bool.isRequired
  },
  render () {
    return (
      <div className={container}>
        <Navigation isAuthed={this.props.isAuthed}/>
        <div className={innerContainer}>
          {this.props.children}
        </div>
      </div>
    )
  }
})

export default withRouter(connect(
  (state) => ({ isAuthed: state.isAuthed })
)(MainContainer))
