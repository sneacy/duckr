import React from 'react'
import createReactClass from 'create-react-class'
import { Home } from 'components'

const HomeContainer = createReactClass({
  render () {
    return (
      <Home />
    )
  }
})

export default HomeContainer
