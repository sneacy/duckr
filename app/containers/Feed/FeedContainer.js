import React from 'react'
import createReactClass from 'create-react-class'
import { Feed } from 'components'

const FeedContainer = createReactClass({
  render () {
    return (
      <Feed />
    )
  }
})

export default FeedContainer
