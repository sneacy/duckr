import { ref, firebaseAuth } from 'config/constants'

export function auth () {
  return firebaseAuth().signInWithPopup(new firebase.auth.FacebookAuthProvider())
  // return new Promise((resolve, reject) => {
  //   setTimeout(() => {
  //     resolve({
  //       name: 'Luke Skywalker',
  //       avatar: 'https://i.imgur.com/M7YU3H7.png',
  //       uid: 'lukeskywalker'
  //     })
  //   }, 750)
  // })
}

export function checkIfAuthed (store) {
  // Ignore firebase
  return store.getState().isAuthed
}

export function logout () {
  return firebaseAuth().signout()
}

export function saveUser (user) {
  return ref.child(`users/${user.ui}`)
    .set(user)
    .then(() => user)
}
