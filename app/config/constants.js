import firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyAOn_xNKl5WEbP96wOw6PCCALtsK78ArpU',
  authDomain: 'spn-test-project.firebaseapp.com',
  databaseURL: 'https://spn-test-project.firebaseio.com',
  projectId: 'spn-test-project',
  storageBucket: 'spn-test-project.appspot.com',
  messagingSenderId: '875607792696'
}

firebase.initializeApp(config)

export const ref = firebase.database().ref()
export const fireaseAuth = firebase.auth
