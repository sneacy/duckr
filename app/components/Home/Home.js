import React from 'react'
import { container, title, slogan } from './styles.css'
// import PropTypes from 'prop-types'

export default function Home (props) {
  return (
    <div className={container}>
      <p className={title}>{'Duckr'}</p>
      <p className={slogan}>{'The real time, could based, yada yada yada...'}</p>
    </div>
  )
}
