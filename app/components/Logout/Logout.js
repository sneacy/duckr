import React from 'react'
import { text } from './styles.css'
// import PropTypes from 'prop-types'

export default function Logout (props) {
  return (
    <div className={text}>{'You are now logged out'}</div>
  )
}
