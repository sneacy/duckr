//what is the minimal representation of our app state as an object?


{
  users: {
    isAuthed,
    isFetching,
    error,
    authedId,
    [uid] : {
      lastUpdated,
      info: {
        name,
        uid,
        avatar
      }
    }
  },
  modal: {
    duck,
    isOpen
  },
  ducks: {
    isFetching,
    error,
    [duckId]: {
      lastUpdated,
      info: {
        avatar,
        duckId,
        name,
        text,
        timestamp,
        uid
      }
    }
  },
  usersDucks: {
    isFetching,
    error,
    [uid] : {
      lastUpdated,
      duckIds: [duckId, duckId...]
    }
  },
  likeCount: {
    [duckId]: 0
  },
  userLikes: {
    [duckId]: true
  },
  replies: {
    isFetching,
    error,
    [duckId]: {
      lastUpdated,
      replies: {
        [replyId]: {
          name,
          comment,
          etc..
        }
      }
    }
  },
  listeners: {
    [listenersId]: true
  },
  feed: {
    isFetching,
    error,
    newDucksAvailable,
    duckIdsToAdd: [duckId, duckId],
    duckIds: [duckIds, duckIds] //already in the feed
  }
},